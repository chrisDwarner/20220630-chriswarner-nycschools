//
//  JPMorganCodingTestApp.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import SwiftUI

@main
struct JPMorganCodingTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
