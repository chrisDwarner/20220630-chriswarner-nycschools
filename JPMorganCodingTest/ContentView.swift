//
//  ContentView.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import SwiftUI

struct ContentView: View {
  @StateObject private var viewModel: ViewModel

  init() {
    // by initializing the StateObject here, we avoid any unessesary calls to the constructor of the ViewModel
    // Any time we call the ViewModel constructor, we would download the list of schools and sat scores, not desirable.
    // during the construction of a View, the init is only called once, while the struct will be parsed many times.
    _viewModel = StateObject<ViewModel>(wrappedValue: ViewModel())
  }

  var body: some View {
    NavigationView {
      List(viewModel.schools.list) { school in
        NavigationLink(destination: SchoolView(model: SchoolModel(school: school)).environmentObject(viewModel)) {
          Text(school.name)
        }
      }.navigationBarTitle("NYC schools", displayMode: .inline)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
