//
//  Sats.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import Combine
import Foundation


struct Sats: Identifiable {
  var id = UUID()
  var list = [Sat]()

  static var iscacheNotLoaded = true
  static var cached = Sats()
  static var publisher: AnyPublisher<Sats, Never> {
    guard iscacheNotLoaded, let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {

      // now instead of returning just the empty Sats.publisher, we return the cached list if we have
      // already fetched it from the server
      if cached.list.isEmpty {
        return Just<Sats>(Sats()).eraseToAnyPublisher()
      }
      return Just<Sats>(cached).eraseToAnyPublisher()
    }

    // download the list of schools using a URLSession datatask publisher.
    return URLSession.shared.dataTaskPublisher(for: url)
      .map { data, response in
        do {
          let decoder = JSONDecoder()
          let results = try decoder.decode([Sat].self, from: data)

          // first time through, cache the the Schools and return.
          let sats = Sats(list: results)
          self.iscacheNotLoaded = false
          self.cached = sats
          return sats
        }
        catch {
          return Sats()
        }
      }
      .replaceError(with: Sats())
      .eraseToAnyPublisher()
  }
}
