//
//  Model.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import Combine
import Foundation

class Model {
  
  var schools: AnyPublisher<Schools, Never> { Schools.publisher }
  var satScores: AnyPublisher<Sats, Never> { Sats.publisher }
}

