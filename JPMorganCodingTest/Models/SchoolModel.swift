//
//  SchoolModel.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import Combine
import Foundation

class SchoolModel: ObservableObject {
  @Published var school: School

  init(school building: School) {
    school = building
  }
}
