//
//  Schools.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import Combine
import Foundation

struct Schools: Identifiable {
  var id = UUID()
  var list = [School]()

  static var iscacheNotLoaded = true
  static var cached = Schools()
  static var publisher: AnyPublisher<Schools, Never> {
    guard iscacheNotLoaded, let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {

      // now instead of returning just the empty Schools.publisher, we return the cached list if we have
      // already fetched it from the server
      if cached.list.isEmpty {
        return Just<Schools>(Schools()).eraseToAnyPublisher()
      }
      return Just<Schools>(cached).eraseToAnyPublisher()
    }

    // download the list of schools using a URLSession datatask publisher.
    return URLSession.shared.dataTaskPublisher(for: url)
      .map { data, response in
        do {
          let decoder = JSONDecoder()
          let schoolList = try decoder.decode([School].self, from: data)

          // first time through, cache the the Schools and return.
          let schools = Schools(list: schoolList)
          self.iscacheNotLoaded = false
          self.cached = schools
          return schools
        }
        catch {
          return Schools()
        }
      }
      .replaceError(with: Schools())
      .eraseToAnyPublisher()
  }

}

