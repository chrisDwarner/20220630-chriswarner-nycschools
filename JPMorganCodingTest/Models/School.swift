//
//  School.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import Foundation

struct School: Identifiable {
  var id: String { dbn }
  var dbn = String()
  var name = String()
  var address = String()
  var city = String()
  var zip = String()
  var state = String()
  var burrough = String()
  var website = String()
  var email = String()
  var phone = String()
}

extension School: Codable {
  enum CodingKeys: String, CodingKey {
    case id
    case dbn = "dbn"
    case name = "school_name"
    case address = "primary_address_line_1"
    case city = "city"
    case zip = "zip"
    case state = "state_code"
    case burrough = "borough"
    case website = "website"
    case email = "school_email"
    case phone = "phone_number"
  }

  init( from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

    dbn = try container.decode(String.self, forKey: .dbn)
    name = try container.decode(String.self, forKey: .name)
    address = try container.decode(String.self, forKey: .address)
    city = try container.decode(String.self, forKey: .city)
    zip = try container.decode(String.self, forKey: .zip)
    state = try container.decode(String.self, forKey: .state)
    burrough = try container.decodeIfPresent(String.self, forKey: .burrough) ?? " - "
    website = try container.decodeIfPresent(String.self, forKey: .website) ?? "N/A"
    phone = try container.decodeIfPresent(String.self, forKey: .phone) ?? "N/A"
    email = try container.decodeIfPresent(String.self, forKey: .email) ?? "N/A"
  }

  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)

    try container.encode(dbn, forKey: .dbn)
    try container.encode(name, forKey: .name)
    try container.encode(address, forKey: .address)
    try container.encode(city, forKey: .city)
    try container.encode(zip, forKey: .zip)
    try container.encode(state, forKey: .state)
    try container.encode(burrough, forKey: .burrough)
    try container.encode(website, forKey: .website)
    try container.encode(phone, forKey: .phone)
    try container.encode(email, forKey: .email)
  }
}
