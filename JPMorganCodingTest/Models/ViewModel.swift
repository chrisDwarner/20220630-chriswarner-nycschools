//
//  ViewModel.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import Combine
import Foundation

class ViewModel: ObservableObject {

  @Published var schools = Schools()
  @Published var satData = Sats()

  private var model = Model()
  private lazy var schoolsPublisher: AnyPublisher< Schools, Never> = {
    $schools.flatMap { schools -> AnyPublisher< Schools, Never> in
        self.model.schools
    }
    .receive(on: DispatchQueue.main)
    .eraseToAnyPublisher()
  }()

  private lazy var satsPublisher: AnyPublisher< Sats, Never> = {
    $satData.flatMap { satData -> AnyPublisher< Sats, Never> in
      self.model.satScores
    }
    .receive(on: DispatchQueue.main)
    .eraseToAnyPublisher()
  }()

  init() {
    schoolsPublisher.assign(to: &$schools )
    satsPublisher.assign(to: &$satData )
  }
}
