//
//  Sat.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import Foundation

struct Sat: Identifiable {
  var id: String { dbn }
  var dbn = String()
  var name = String()
  var numTestTakers = String()
  var criticalReadingAvg = String()
  var mathAvg = String()
  var writingAvg = String()
}

extension Sat: Codable {
  enum CodingKeys: String, CodingKey {
    case id
    case dbn = "dbn"
    case name = "school_name"
    case numTestTakers = "num_of_sat_test_takers"
    case criticalReadingAvg = "sat_critical_reading_avg_score"
    case mathAvg = "sat_math_avg_score"
    case writingAvg = "sat_writing_avg_score"
  }

  init( from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

    dbn = try container.decode(String.self, forKey: .dbn)
    name = try container.decode(String.self, forKey: .name)
    numTestTakers = try container.decodeIfPresent(String.self, forKey: .numTestTakers) ?? "0"
    criticalReadingAvg = try container.decodeIfPresent(String.self, forKey: .criticalReadingAvg) ?? "0"
    mathAvg = try container.decodeIfPresent(String.self, forKey: .mathAvg) ?? "0"
    writingAvg = try container.decodeIfPresent(String.self, forKey: .writingAvg) ?? "0"

    #if DEBUG
    print("\(dbn) - \(name) \(numTestTakers)")
    #endif
  }

  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)

    try container.encode(dbn, forKey: .dbn )
    try container.encode(name, forKey: .name )
    try container.encode(numTestTakers, forKey: .numTestTakers )
    try container.encode(criticalReadingAvg, forKey: .criticalReadingAvg )
    try container.encode(mathAvg, forKey: .mathAvg )
    try container.encode(writingAvg, forKey: .writingAvg )
  }

}
