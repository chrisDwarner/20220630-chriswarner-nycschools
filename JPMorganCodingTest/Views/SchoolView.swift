//
//  SchoolView.swift
//  JPMorganCodingTest
//
//  Created by chris warner on 6/30/22.
//

import SwiftUI

struct SchoolView: View {
  @EnvironmentObject var viewModel: ViewModel
  @StateObject var model: SchoolModel

  var body: some View {
    VStack(alignment: .leading) {
      schoolInfo()
      Divider()
      contactInfo()
      Divider()
      Text("Sat Scores").font(.title2)
      satContent()
      Spacer()
    }.navigationBarTitle(model.school.name, displayMode: .inline)
      .padding()
  }

  @ViewBuilder
  func schoolInfo() -> some View {
    rowItem(title: "DBN", value: model.school.dbn)
    rowItem(title: "Name", value: model.school.name)
    rowItem(title: "Address", value: model.school.address)
    rowItem(title: "City", value: model.school.city)
    rowItem(title: "State", value: model.school.state)
    rowItem(title: "Burrough", value: model.school.burrough)
  }

  @ViewBuilder
  func contactInfo() -> some View {
    Text("Contact info").font(.title2)
    rowItem(title: "Phone", value: model.school.phone)
    rowItem(title: "Email", value: model.school.email)
    HStack {
      Text("Web Site:").bold()
      Text(model.school.website)
        .foregroundColor(.gray)
        .font(.body)
        .truncationMode(.tail)
        .lineLimit(1)
        .padding()
        .onTapGesture {
          let url = URL.init(string: "https://\(model.school.website)")
          guard let website = url, UIApplication.shared.canOpenURL(website) else { return }
          UIApplication.shared.open(website)
        }
    }

  }

  @ViewBuilder
  func satContent() -> some View {
    if let sat = viewModel.satData.list.filter({ $0.dbn == model.school.dbn}).first {
      rowItem(title: "Test Takers", value: sat.numTestTakers)
      rowItem(title: "Critical Reading Avg", value: sat.criticalReadingAvg)
      rowItem(title: "Math Avg", value: sat.mathAvg)
      rowItem(title: "Writing Avg", value: sat.writingAvg)
    }
  }

  @ViewBuilder
  func rowItem(title: String, value: String) -> some View {
    HStack {
      Text("\(title):").bold()
      Text(value)
        .foregroundColor(.gray)
        .font(.body)
        .truncationMode(.tail)
        .lineLimit(1)
    }
  }
}

struct SchoolView_Previews: PreviewProvider {
  static var previews: some View {
    SchoolView(model: SchoolModel(school:School()))
  }
}
