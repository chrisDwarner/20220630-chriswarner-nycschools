//
//  JPMorganCodingTestTests.swift
//  JPMorganCodingTestTests
//
//  Created by chris warner on 6/30/22.
//

import XCTest
@testable import JPMorganCodingTest

class JPMorganCodingTestTests: XCTestCase {
  var viewModel = ViewModel()

  override func setUp() async throws {
    try await super.setUp()
  }

  override func tearDown() {
  }

  func testSchoolsModel() throws {
    XCTAssertFalse(viewModel.schools.list.count != 0, file: "found the list of schools")
  }

  func testSatModel() throws {
    XCTAssertFalse(viewModel.satData.list.count != 0, file: "found the list of schools Sat Data")
  }
}
